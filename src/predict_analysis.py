import argparse
import os
import random

import PIL
import matplotlib.pyplot as plt
import pydot
import torch
from torchvision import transforms
from torchviz import make_dot

parser = argparse.ArgumentParser("Prediction")

parser.add_argument('--path', type=str, default='datasets/tumor_segmentation')

parser.add_argument('--model_path', type=str, default='models/deeplabv3plus_80.pt', help="model path")

parser.add_argument('--gpu', type=str, default='', help='select the gpu or gpus to use')

args = parser.parse_args()


def summary(model):
    model.eval()
    x = torch.zeros(1, 3, 32, 32, dtype=torch.float, requires_grad=False)
    out = model(x)
    dot = make_dot(out, params=dict(model.named_parameters()))
    graphs = pydot.graph_from_dot_data(str(dot))
    graphs[0].write_svg('models/model.svg')


if __name__ == '__main__':
    images_dir = os.path.join(args.path, 'train/images')
    mask_dir = os.path.join(args.path, 'train/labels')

    names = os.listdir(images_dir)
    random_image = random.choice(names)

    image_path = os.path.join(images_dir, random_image)
    mask_path = os.path.join(mask_dir, random_image)

    image_shape = (64, 64)
    data_transform = transforms.Compose([
        transforms.Resize(image_shape),
        transforms.ToTensor(),
        # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])
    image = PIL.Image.open(image_path).convert('RGB')
    tensor_image = data_transform(image)
    tensor_images = tensor_image.reshape(1, 3, 64, 64)

    target_transform = transforms.Compose([
        transforms.Resize(image_shape),
        transforms.ToTensor(),
    ])
    mask = PIL.Image.open(mask_path)
    tensor_mask = target_transform(mask)
    tensor_masks = tensor_mask.reshape(1, 1, 64, 64)

    model = torch.load(args.model_path)
    summary(model)

    model.eval()
    pred = model(tensor_images)

    f, axarr = plt.subplots(1, 3)
    f.suptitle(f'Title', fontsize=16)

    axarr[0].imshow(transforms.ToPILImage()(tensor_images[0].cpu()))
    axarr[0].set_title('image')

    axarr[1].imshow(transforms.ToPILImage()(tensor_masks[0].cpu()))
    axarr[1].set_title('mask')

    axarr[2].imshow(transforms.ToPILImage()(pred[0].cpu()))
    axarr[2].set_title('predicted')

    plt.show()
    # plt.savefig('models/pred.svg')
