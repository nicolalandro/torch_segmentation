from typing import Union

import torch
from ignite.exceptions import NotComputableError


class Average:
    def __init__(self):
        self.reset()

    def reset(self) -> None:
        self._sum = 0.0
        self._num_examples = 0
        self.output = 0

    def update(self, output) -> None:
        self.output = output
        self._sum += torch.sum(output).item()
        self._num_examples += output.shape[0]

    def compute(self) -> Union[float, torch.Tensor]:
        if self._num_examples == 0:
            raise NotComputableError("Average must have at least one example before it can be computed.")
        return self._sum / self._num_examples

    def item(self):
        return self.output.item()


if __name__ == '__main__':
    average = Average()

    for x in [[2], [6], [3]]:
        average.update(torch.tensor(x))
        print(average.item(), average.compute())
