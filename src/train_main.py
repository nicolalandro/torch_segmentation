import argparse
import datetime
import os
import time

import torch
from torch import nn
from torch.optim import lr_scheduler

from src.custom_datasets.simpli_dataset import get_dataset_loaders
from src.custom_metrics.average import Average
from src.custom_models.deep_lab_v3_plus import DeepLabv3Plus

parser = argparse.ArgumentParser("Train")

# dataset
parser.add_argument('--path', type=str, default='datasets/tumor_segmentation')

parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.01, help="learning rate for model")
parser.add_argument('--stepsize', type=int, default=50)
parser.add_argument('--gamma', type=float, default=0.1, help="learning rate decay")

parser.add_argument('--model', type=str, default='deeplabv3plus',
                    choices=['deeplabv3plus'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default='10', help="save model at epoch x")
parser.add_argument('--save-dir', type=str, default='models')

parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

args = parser.parse_args()


def main():
    torch.manual_seed(args.seed)

    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')
    device = handle_device()

    image_shape = (64, 64)
    trainloader = get_dataset_loaders(args.path, args.batch_size)
    testloader = trainloader

    num_classes = 1
    model = create_model(args.model, num_classes, image_shape)
    model = nn.DataParallel(model).to(device)

    criterion = nn.MSELoss().cuda()

    params = list(model.parameters())
    optimizer = torch.optim.Adam(params, args.lr)

    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))

        train_model(model, device, trainloader, criterion, optimizer)

        scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        test_model(model, device, testloader, criterion, optimizer)

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model, f'{args.save_dir}/{args.model}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train_model(model, device, trainloader, criterion, optimizer):
    model.train()
    loss_average = Average()

    for batch_idx, (data, labels) in enumerate(trainloader):
        data = data.to(device, non_blocking=True)
        labels = labels.to(device, non_blocking=True)

        predictions = model(data)

        loss = criterion(predictions, labels)

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        loss_average.update(torch.tensor([loss.item()]))

        if (batch_idx + 1) % 1 == 0:
            print(f"Batch {batch_idx + 1}\t  Loss {loss_average.item()} ({loss_average.compute()})")


def test_model(model, device, testloader, criterion, optimizer):
    model.eval()

    loss_average = Average()
    with torch.no_grad():
        for data, labels in testloader:
            labels = labels.to(device, non_blocking=True)
            data = data.to(device, non_blocking=True)

            predictions = model(data)

            loss = criterion(predictions, labels)
            loss_average.update(torch.tensor([loss.item()]))

            # compute jaccard

    print(f'Test Loss(average): {loss_average.compute()}')


def create_model(model_name, num_classes, image_shape):
    if model_name == 'deeplabv3plus':
        model = DeepLabv3Plus(num_classes=num_classes)
    return model


def handle_device():
    if args.gpu == '':
        device = torch.device('cpu')
        print(f"Currently using cpu")
    else:
        device = torch.device('cuda')
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            print(f"Currently using GPU: {args.gpu}")
        else:
            print("No gpu is detected, run with --gpu ''")
            exit()
    return device


if __name__ == '__main__':
    main()
