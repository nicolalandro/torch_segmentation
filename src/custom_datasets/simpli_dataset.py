import os
from PIL import Image

import torch
import torchvision
import torchvision.transforms as transforms


class SegmentationFolder(torchvision.datasets.VisionDataset):
    def __init__(self,
                 root,
                 image_set='train',
                 transform=None,
                 target_transform=None,
                 transforms=None):
        super(SegmentationFolder, self).__init__(root, transforms, transform, target_transform)

        image_dir = os.path.join(root, image_set, 'images')
        mask_dir = os.path.join(root, image_set, 'labels')

        self.images = [os.path.join(image_dir, filename) for filename in sorted(os.listdir(image_dir))]
        self.masks = [os.path.join(mask_dir, filename) for filename in sorted(os.listdir(image_dir))]
        assert (len(self.images) == len(self.masks))

    def __getitem__(self, index):
        img = Image.open(self.images[index]).convert('RGB')
        target = Image.open(self.masks[index]).convert('RGB')

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.images)


def get_dataset_loaders(path, batch_size):
    image_shape = (64, 64)
    data_transform = transforms.Compose([
        transforms.Resize(image_shape),
        transforms.ToTensor(),
        # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])

    target_transform = transforms.Compose([
        transforms.Resize(image_shape),
        transforms.ToTensor(),
    ])

    imageset = SegmentationFolder(root=path, transform=data_transform, target_transform=target_transform)
    # print(.__getitem__(0))
    imageloader = torch.utils.data.DataLoader(imageset, batch_size=batch_size, shuffle=True, num_workers=2)

    return imageloader


if __name__ == '__main__':
    batch_size = 1
    path = 'datasets/tumor_segmentation/'

    imageloader = get_dataset_loaders(path, batch_size)
    # print('num classes:', num_classes)

    device = torch.device('cpu')
    for epoch in range(2):
        for batch_idx, (inputs, targets) in enumerate(imageloader):
            inputs, targets = inputs.to(device), targets.to(device)
            print('train input', inputs.shape)
            # print(epoch, inputs[0][0])
            print('train output', targets.shape)
