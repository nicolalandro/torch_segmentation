# Train a segmentation model

## Study
[Review of Deep Learning Algorithms for Image Semantic Segmentation](https://medium.com/@arthur_ouaknine/review-of-deep-learning-algorithms-for-image-semantic-segmentation-509a600f7b57)

## exec
```bash
pip install -r requiremenst
python3 src/train_main.py
python3 src/predict_analysis.py
```